import pymysql
from tkinter import *
#from tkinter import ttk
import tkinter.messagebox as msgbox
import random

#Conecto a mysql
conexion =pymysql.connect(host='127.0.0.1',port=3306,user='root', password="lolaisql2412*" )
cursor = conexion.cursor()

#CREO BASE DE DATOS Y TABLAS 
try:
    with cursor:
        # base de datos
        cursor.execute("CREATE DATABASE IF NOT EXISTS final")

        cursor.execute("USE final")
        
        # tabla de clientes
        cursor.execute("""
            CREATE TABLE IF NOT EXISTS usuarios (
                nombre VARCHAR(200) NOT NULL,
                correo VARCHAR(200) NOT NULL,
                contrasena VARCHAR(200) NOT NULL
            )
        """)
        
        #tabla de viajes
        cursor.execute("""
            CREATE TABLE IF NOT EXISTS viajes (
                origen VARCHAR(200) NOT NULL,
                destino VARCHAR(200) NOT NULL,
                fecha VARCHAR(200) NOT NULL,
                tipo  VARCHAR(200) NOT NULL,
                precio VARCHAR(200) NOT NULL,
                horario VARCHAR(200) NOT NULL,
                disponibilidad VARCHAR(200) NOT NULL
            )
        """)
        
    conexion.commit()
    
except pymysql.Error as e:
    print(f"Error al crear la base de datos y tabla: {e}")
    
finally:
    conexion.close()


#CARGAR DATOS RANDOM
#Conecto a mysql
conexion =pymysql.connect(host='127.0.0.1',port=3306,user='root', password="lolaisql2412*",database='final' )
cursor = conexion.cursor()

# Uso faker para simular datos de cliente y de los viajes y los agrego a la tabla de la base de datos 
tipo_de_coche = ["cama", "semicama"]

from faker import Faker
fake=Faker('es_AR')
datos_cliente=[]
datos_empresa=[]
for _ in range(100):
    datos_cliente.append((fake.name(),fake.email(),fake.password()))
    datos_empresa.append((fake.city(),fake.city(),fake.date(),random.choice(tipo_de_coche),random.randint(5000, 15000),fake.time(), random.randint(0, 50)))

cursor.execute('Select * from usuarios')
if cursor.rowcount==0:
    agregar1 = f"INSERT INTO usuarios (nombre, correo, contrasena) VALUES (%s, %s, %s)"
    cursor.executemany(agregar1, datos_cliente)
    
cursor.execute('Select * from viajes')
if cursor.rowcount==0:  
    agregar2 = f"INSERT INTO viajes (origen,destino,fecha, tipo, precio, horario, disponibilidad) VALUES (%s, %s, %s, %s, %s, %s,%s)"
    cursor.executemany(agregar2, datos_empresa)
    conexion.commit()


#datos para probar como funciona el codig con viajes repetidos
hola=['Mendoza','Chilecito','2020-10-13','cama','8000','12:00:00','3']
agregar2 = f"INSERT INTO viajes (origen,destino,fecha, tipo, precio, horario,disponibilidad) VALUES (%s, %s, %s, %s, %s, %s,%s)"
cursor.execute(agregar2, hola)
conexion.commit()

hola1=['Mendoza','Chilecito','2020-10-13','cama','7000','11:00:00','10']
agregar3 = f"INSERT INTO viajes (origen,destino,fecha, tipo, precio, horario,disponibilidad) VALUES (%s, %s, %s, %s, %s, %s,%s)"
cursor.execute(agregar3, hola1)
conexion.commit()
