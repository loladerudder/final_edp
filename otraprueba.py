from tkinter import *
import tkinter.messagebox as msgbox
import pymysql
from PIL import Image, ImageTk
import tkinter as tk
from tkinter import ttk

class Registro:
    def __init__(self, master):
        #Creo ventana
        self.master = master
        master.title('Registro')
        master.geometry('925x500')
        master.configure(bg='#fff')
        master.resizable(False, False)

        # Agregar foto y agrandarla
        self.img=PhotoImage(file='colec1.png')
        Label(master,image=self.img, width=950, height=500,border=0,bg='white').place(x=0,y=0)

        # Agregar recuadro y título
        self.recuadro = Frame(master, width=400, height=400, bg='white')
        self.recuadro.place(x=480, y=50)
        titulo = Label(self.recuadro, text='Regístrate!', bg='white', fg='red3', font=('Mincrosoft Yahei UI Light', 23, 'bold'))
        titulo.place(x=115, y=20)

        
        #Creo cajas de ingreso con una funcion generalizada
        self.caja_usuario = self.crear_caja(self.recuadro, 170, 91, 25,'Nombre y Apellido:',30,200,None)
        self.caja_email = self.crear_caja(self.recuadro, 84, 141, 34,'E-mail:', 30,286,None)
        self.caja_contrasena = self.crear_caja(self.recuadro, 122, 191, 30,'Contraseña:', 30,246,'*')
        self.caja_confirmar_contra = self.crear_caja(self.recuadro, 192, 260, 25,'Confirmar contraseña:',30,174,'*')
        
        # Boton de registro vs. iniciar sesión
        Button(self.recuadro, width=39, pady=6, text='Regístrate', bg='red3', fg='white', border=0,command=self.registro, font=('Mincrosoft Yahei UI Light', 9)).place(x=55, y=310)
        texto_inicio_sesion = Label(self.recuadro, text='Ya tengo cuenta.', bg='white', fg='dim gray',font=('Mincrosoft Yahei UI Light', 9)).place(x=100, y=345)
        Button(self.recuadro, width=10, pady=1, text='Iniciar sesión', bg='white', fg='red3', border=0,command=self.inicio_de_sesion, font=('Mincrosoft Yahei UI Light', 9,'underline')).place(x=200, y=345)
        Button(self.recuadro, width=20, pady=1, text='Mostrar contraseña', bg='white', fg='dim gray', border=0,command=self.mostrar_contra, font=('Mincrosoft Yahei UI Light', 9,'underline')).place(x=50, y=220)

    def crear_caja(self,donde , x1, y1, ancho1,texto,x2,ancho2,mostrar):
        caja = Entry(donde, width=ancho1, fg='black', border=0, bg='white', show=mostrar, font=('Mincrosoft Yahei UI Light', 11))
        caja.place(x=x1, y=y1)
        texto_de_caja=Label(donde, text=texto, bg='white', fg='dim gray', font=('Mincrosoft Yahei UI Light', 11, 'bold')).place(x=x2, y=y1-1)
        linea=Frame(donde, width=ancho2, height=1, bg='dim gray').place(x=x1, y=y1+19)
        return caja, texto_de_caja,linea

    def registro(self):
        conexion =pymysql.connect(host='127.0.0.1',port=3306,user='root', password="lolaisql2412*",database='final' )
        cursor = conexion.cursor()
        
        usu = self.caja_usuario[0].get()
        email = self.caja_email[0].get()
        contrasena = self.caja_contrasena[0].get()
        confirmar_contra = self.caja_confirmar_contra[0].get()

        if contrasena is not None and confirmar_contra is not None and contrasena == confirmar_contra and contrasena != "":
            try:
                with cursor:
                    sql = 'INSERT INTO usuarios (nombre, correo, contrasena) VALUES (%s, %s, %s)'
                    cursor.execute(sql, (usu, email, contrasena))
                conexion.commit()
                msgbox.showinfo('Registro', 'Registrado exitosamente!')
                Registro(self.master)
                
            except pymysql.Error as e:
                print(f"Error al agregar datos: {e}")
            finally:
                conexion.close()
        elif contrasena== "" or confirmar_contra== "" or email== "" or usu== "":
            msgbox.showerror('Registro', 'Campos incompletos.')
        else:
            msgbox.showerror('Registro', 'Las contraseñas no coinciden. Ingresar nuevamente.')

        pass
    
    def mostrar_contra(self):
        contra= self.caja_contrasena[0].get()
        self.ver_contra=Label(self.recuadro, text=contra, bg='white', fg='dim gray',font=('Mincrosoft Yahei UI Light', 9)).place(x=200, y=220)
        return self.ver_contra   
    
    def inicio_de_sesion(self):
        self.recuadro.destroy()
        InicioSesion(self.master)

class InicioSesion:
    def __init__(self, master):
        self.master = master
        master.title('Inicio de sesión')
        master.geometry('925x500')
        master.configure(bg='#fff')
        master.resizable(False, False)

        # Agregar foto y agrandarla
        self.img2=PhotoImage(file='colec2.png')
        Label(master,image=self.img2, width=925, height=500,border=0,bg='white').place(x=0,y=0)

       
        # Agregar recuadro y título
        self.recuadro2 = Frame(master, width=400, height=300, bg='white')
        self.recuadro2.place(x=480, y=50)
        titulo2 = Label(self.recuadro2, text='Inicia sesión!', bg='white', fg='navy', font=('Mincrosoft Yahei UI Light', 23, 'bold'))
        titulo2.place(x=95, y=20)

        #Crear cajas con funcion
        self.caja_usuario_existente = self.crear_caja(self.recuadro2, 170, 91, 25,'Nombre y Apellido:',30,200,None)
        self.caja_contrasena_existente = self.crear_caja(self.recuadro2, 122, 141, 30,'Contraseña:',30,250,'*')

        #Botones
        Button(self.recuadro2, width=39, pady=6, text='Iniciar sesión', bg='navy', fg='white', border=0, command=self.verifico,font=('Mincrosoft Yahei UI Light', 9)).place(x=65, y=220)
        Button(self.recuadro2, width=20, pady=1, text='Mostrar contraseña', bg='white', fg='dim gray', border=0,command=self.mostrar_contra, font=('Mincrosoft Yahei UI Light', 9,'underline')).place(x=50, y=180)

    def crear_caja(self,donde, x1, y1, ancho1,texto,x2,ancho2,mostrar):
        caja = Entry(donde, width=ancho1, fg='black', border=0, bg='white', show=mostrar, font=('Mincrosoft Yahei UI Light', 11))
        caja.place(x=x1, y=y1)
        texto_de_caja=Label(donde, text=texto, bg='white', fg='dim gray', font=('Mincrosoft Yahei UI Light', 11, 'bold')).place(x=x2, y=y1-1)
        linea=Frame(donde, width=ancho2, height=1, bg='dim gray').place(x=x1, y=y1+19)
        return caja, texto_de_caja,linea
    
    def mostrar_contra(self):
        contra= self.caja_contrasena_existente[0].get()
        self.ver_contra=Label(self.recuadro2, text=contra, bg='white', fg='dim gray',font=('Mincrosoft Yahei UI Light', 9)).place(x=200, y=180)
        return self.ver_contra   
    
    def verifico(self):
        conexion =pymysql.connect(host='127.0.0.1',port=3306,user='root', password="lolaisql2412*",database='final' )
        cursor = conexion.cursor()
        
        usu_existente = self.caja_usuario_existente[0].get().lower()
        contrasena_existente = self.caja_contrasena_existente[0].get()

        consulta = "SELECT * FROM usuarios WHERE LOWER(nombre) = %s AND contrasena = %s"
        datos = (usu_existente, contrasena_existente)

        try:
            with cursor:
                cursor.execute(consulta, datos)
                resultado = cursor.fetchone()
                
            if resultado:
                pass
                self.recuadro2.destroy()
                PaginaPrincipal(self.master)   
            else:
                msgbox.showerror("Inicio de sesión", "La cuenta o la contraseña con incorrectas. Intente nuevamente o regístrese.")
                
        except pymysql.Error as e:
            print(f"Error al consultar datos: {e}")
        finally:
            conexion.close()

    pass
         
class PaginaPrincipal:
    
    def __init__(self, master):
        self.master = master
        master.title('Viajes ')
        master.geometry('520x450')
        master.configure(bg='#fff')
        master.resizable(False, False)

        # Agregar foto y agrandarla
        self.img2=PhotoImage(file='colec2.png')
        Label(master,image=self.img2, width=925, height=600,border=0,bg='white').place(x=0,y=0)

        # Agregar recuadro y título
        self.recuadro1 = Frame(master, width=380, height=350, bg='white')
        self.recuadro1.place(x=70, y=50)
        self.titulo1 = Label(self.recuadro1, text='Elige tu viaje!', bg='white', fg='navy', font=('Mincrosoft Yahei UI Light', 23, 'bold'))
        self.titulo1.place(x=90, y=30)
       
        # Cajas
        self.caja_origen = self.crear_caja(self.recuadro1,92,101,25,'Origen:',30,88,250)
        self.caja_destino = self.crear_caja(self.recuadro1, 94,141,25,'Destino:',30,92,246)
        self.caja_fecha1 = self.crear_caja(self.recuadro1, 250,181,5,'Fecha(día/mes/año):',30,250,40)
        self.caja_fecha2 = self.crear_caja(self.recuadro1, 215,181,3,'/',205,215,25)
        self.caja_fecha3 = self.crear_caja(self.recuadro1, 180,181,3,'/',240,180,25)

        # Boton de buscar viajes
        Button(self.recuadro1, width=29, pady=6, text='Buscar mi viaje', bg='navy', fg='white', border=0, command=self.buscar_viajes, font=('Mincrosoft Yahei UI Light', 9)).place(x=85, y=260)
    
    def crear_caja(self,donde , x1, y1, ancho1,texto,x2,x3,ancho2):
        caja = Entry(donde, width=ancho1, fg='black', border=0, bg='white', font=('Mincrosoft Yahei UI Light', 11))
        caja.place(x=x1, y=y1)
        texto_de_caja=Label(donde, text=texto, bg='white', fg='dim gray', font=('Mincrosoft Yahei UI Light', 11, 'bold')).place(x=x2, y=y1-1)
        linea=Frame(donde, width=ancho2, height=1, bg='dim gray').place(x=x3, y=y1+19)
        return caja, texto_de_caja,linea
    
    def buscar_viajes(self):
        conexion =pymysql.connect(host='127.0.0.1',port=3306,user='root', password="lolaisql2412*",database='final' )
        cursor = conexion.cursor()
        
        ciudad_origen = self.caja_origen[0].get().lower()
        ciudad_destino = self.caja_destino[0].get().lower()
        dia=self.caja_fecha3[0].get()
        mes=self.caja_fecha2[0].get()
        ano=self.caja_fecha1[0].get()
        fecha = ano+'-'+mes+'-'+dia

        consulta_viaje = "SELECT * FROM viajes WHERE LOWER(origen) = %s AND LOWER(destino) = %s AND fecha = %s"
        datos_viaje = (ciudad_origen, ciudad_destino,fecha)

        try:
            with cursor:
                cursor.execute(consulta_viaje, datos_viaje)
                resultado_viaje = cursor.fetchall()

            if resultado_viaje:
                opciones_precios = []
                opciones_tipos = []
                opciones_horarios=[]
                opciones_asientos = []
                for resu in resultado_viaje:
                    precio= resu[4]
                    opciones_precios.append(precio)
                    tipo= resu[3]
                    opciones_tipos.append(tipo)
                    horario= resu[5]
                    opciones_horarios.append(horario)
                    asiento= resu[6]
                    opciones_asientos.append(asiento)
                
                
                pass
                ViajesDisponibles(self.master, ciudad_origen, ciudad_destino, ano,mes, dia,opciones_precios,opciones_tipos, opciones_horarios,opciones_asientos)
                
                
            else:
                msgbox.showerror("Viaje", "No hay viaje disponible")
                
        except pymysql.Error as e:
            print(f"Error al consultar datos: {e}")
        finally:
            conexion.close()
    
    pass
    
class ViajesDisponibles:
    def __init__(self, master, ciudad_origen, ciudad_destino, ano, mes, dia,opciones_precios,opciones_tipos, opciones_horarios,opciones_asientos):
        self.master = master
        master.title('Comprar viaje ')
        master.geometry('500x900')
        master.configure(bg='white')
        master.resizable(True,True)

        # Agregar foto y agrandarla
        self.img2=PhotoImage(file='colec2.png')
        Label(master,image=self.img2, width=980, height=650,border=0,bg='white').place(x=0,y=0)

        # Agregar recuadro y título
        self.recuadro3 = Frame(master, width=410, height=230, bg='navy')
        self.recuadro3.place(x=40, y=30)
        self.titulo3 = Label(self.recuadro3, text='Tu búsqueda:', bg='navy', fg='white', font=('Mincrosoft Yahei UI Light', 18, 'bold'))
        self.titulo3.place(x=25, y=10)
        
        import tkinter as tk
        #veo la cantidad de opciones que tengo para el origen, destino y fecha ingreados
        cantidad_opciones=len(opciones_precios)
        
        for i in range(cantidad_opciones):
            self.recuadro_agrego = Frame(master, width=410, height=200, bg='white')
            self.recuadro_agrego.place(x=40, y=85+(i*165))
            self.canvas = tk.Canvas(self.recuadro_agrego,bg='white', highlightbackground='navy', border=0,width=350, height=140)
            self.canvas.place(x=26, y=15)
            self.canvas.create_oval(20-5, 18 - 5, 20 + 5, 18 + 5, outline="dim gray", fill="red3", width=2)
            self.canvas.create_oval(20-5, 63 - 5, 20 + 5, 63 + 5, outline="dim gray", fill="red3",width=2)
            self.canvas.create_line(20,20,20 ,57, dash=(3,3), width=2, fill='dim gray')
            texto_origen= self.crear_label(self.canvas,28,10,'gray20', ciudad_origen.upper(),11)
            texto_destino= self.crear_label(self.canvas,28,50,'gray20', ciudad_destino.upper(),11)
            texto_fecha= self.crear_label(self.canvas,208,10,'navy', dia+'/'+mes+'/'+ano,18)
            texto_horario= self.crear_label(self.canvas,208,40,'navy', opciones_horarios[i],14)
            texto_precio= self.crear_label(self.canvas,20,110,'gray20', 'Precio: '+'$'+opciones_precios[i],11)
            texto_tipo= self.crear_label(self.canvas,19,80,'gray20', 'Tipo de viaje: '+opciones_tipos[i],11)
            texto_asientos=Label(self.canvas, text='Quedan:'+' '+opciones_asientos[i],bg='white', fg='gray20', font=('Mincrosoft Yahei UI Light', 11)).place(x=260, y=75)
            Button(self.canvas,width=10, pady=4, text='COMPRAR', bg='red3', fg='white', border=0, command=lambda op=opciones_asientos[i]: self.comprar(ciudad_origen, ciudad_destino, op), font=('Mincrosoft Yahei UI Light', 9)).place(x=260, y=105)
     
    def crear_label(self,donde, x1, y1, color,texto,tamano):
        label=Label(donde, text=texto, bg='white', fg=color, font=('Mincrosoft Yahei UI Light', tamano)).place(x=x1, y=y1)
        return label
       
    def comprar(self,ciudad_origen,ciudad_destino,asientos):
        if int(asientos)==0:
            msgbox.showwarning('Comprar','No hay más pasajes disponibles. Seleccione otra opción.')
        else:
            ComprarPasaje(self.master,ciudad_origen,ciudad_destino,asientos)

class ComprarPasaje:
    def __init__(self, master,ciudad_origen,ciudad_destino,asientos):
        self.master = master
        master.title('Comprar viaje ')
        master.geometry('980x980')
        master.configure(bg='white')
        master.resizable(True,True)
        self.asientos=asientos
        self.ciudad_origen=ciudad_origen
        self.ciudad_destino=ciudad_destino
        
        # Agregar recuadro y título
        self.canvas = tk.Canvas(master,bg='white', highlightbackground='red3', border=0,width=480, height=250)
        self.canvas.place(x=480, y=50)   
        self.titulo3 = Label(self.canvas, text='Comprar viaje:', bg='white', fg='red3', font=('Mincrosoft Yahei UI Light', 18, 'bold'))
        self.titulo3.place(x=10, y=20)     
        
        #cajas con funcion
        self.caja_nombre=self.crear_caja(self.canvas,90,70,15,'Nombre:',15)
        self.caja_apellido=self.crear_caja(self.canvas,315,70,15,'Apellido:',245)
        self.caja_dni=self.crear_caja(self.canvas,55,110,15,'DNI:',10)
        self.caja_dia=self.crear_caja(self.canvas,270,150,3,'Fecha de nacimiento (dia/mes/año):',15)
        self.caja_mes=self.crear_caja(self.canvas,315,150,3,'/',301)
        self.caja_ano=self.crear_caja(self.canvas,360,150,5,'/',345)
        
        
        opciones_pago = ["Tarjeta", "Efectivo"]

        # Variable para almacenar la opción seleccionada
        opcion_seleccionada = tk.StringVar(self.canvas)

        # Crear el desplegable con opciones
        self.texto_pago = Label(self.canvas, text='Método de pago:', bg='white', fg='dim gray', font=('Mincrosoft Yahei UI Light', 11, 'bold')).place(x=15, y=190)        
        estilo_desplegable = ttk.Style()
        estilo_desplegable.configure("TCombobox",selectbackground="white", font=('Mincrosoft Yahei UI Light', 11))  
        self.desplegable = ttk.Combobox(self.canvas, values=opciones_pago,textvariable=opcion_seleccionada, style="TCombobox")
        self.desplegable.place(x=145,y=190)
    
        Button(self.canvas,width=10, pady=2, text='Continuar', bg='red3', fg='white', command=self.continuar,border=0, font=('Mincrosoft Yahei UI Light', 9)).place(x=305, y=190)
    
    def continuar(self):
        nombre=self.caja_nombre[0].get()
        apellido=self.caja_apellido[0].get()
        dni=self.caja_dni[0].get()
        dia=self.caja_dia[0].get()
        mes=self.caja_mes[0].get()
        ano=self.caja_ano[0].get()
        opcion=self.desplegable.get()
        if opcion=='Efectivo':
            msgbox.showinfo('Compra','Si desea pagar en efectivo debe acercarse a boletería')
        elif nombre== "" or apellido== "" or dni== "" or dia== "" or mes== "" or ano== "" or opcion== "":
            msgbox.showerror('Compra', 'Campos incompletos.')
        elif opcion=='Tarjeta':
            self.canvas2 = tk.Canvas(self.master,bg='white', highlightbackground='red3', border=0,width=480, height=250)
            self.canvas2.place(x=480, y=320)   
            self.titulo3 = Label(self.canvas2, text='Ingresar datos de tarjeta:', bg='white', fg='red3', font=('Mincrosoft Yahei UI Light', 14, 'bold'))
            self.titulo3.place(x=10, y=20)     
            
            #cajas con funcion
            self.caja_nombre=self.crear_caja(self.canvas2,90,70,15,'Nombre:',15)
            self.caja_apellido=self.crear_caja(self.canvas2,315,70,15,'Apellido:',245)
            self.caja_numero=self.crear_caja(self.canvas2,155,110,15,'Número de tarjeta:',15)
            self.caja_dia=self.crear_caja(self.canvas2,280,150,3,'Fecha de vencimiento (dia/mes/año):',15)
            self.caja_mes=self.crear_caja(self.canvas2,325,150,3,'/',311)
            self.caja_ano=self.crear_caja(self.canvas2,370,150,5,'/',355)
            self.caja_codigo=Entry(self.canvas2, width=8, fg='black', border=1, bg='white', show='*',font=('Mincrosoft Yahei UI Light', 11))
            self.caja_codigo.place(x=175, y=190)
            self.texto_de_codigo=Label(self.canvas2, text='Código de Seguridad:', bg='white', fg='dim gray', font=('Mincrosoft Yahei UI Light', 11, 'bold')).place(x=15, y=190)
            Button(self.canvas2,width=10, pady=2, text='Listo!', bg='red3', fg='white', command=self.listo,border=0, font=('Mincrosoft Yahei UI Light', 9)).place(x=305, y=190)
    
        pass   
    
    def listo(self):
        nombre=self.caja_nombre[0].get()
        apellido=self.caja_apellido[0].get()
        numero=self.caja_numero[0].get()
        dia=self.caja_dia[0].get()
        mes=self.caja_mes[0].get()
        ano=self.caja_ano[0].get()
        codigo=self.caja_codigo.get()
        asientos=self.asientos
        if nombre== "" or apellido== "" or numero== "" or dia== "" or mes== "" or ano== "" or codigo== "":
            msgbox.showerror('Compra', 'Campos incompletos.')
        else:
            msgbox.showinfo('Comprar','Compra realizada! Se le enviara la factura y el pasaje a su correo. Gracias por confiar en nosotros!')
            #modificar cantidad de asientos disponibles en la base de datos
            conexion =pymysql.connect(host='127.0.0.1',port=3306,user='root', password="lolaisql2412*",database='final' )
            cursor = conexion.cursor()
            actualizar = "UPDATE viajes SET disponibilidad = %s WHERE disponibilidad = %s"
            asientos_antes=asientos
            asientos_ahora=str(int(asientos)-1)
            cursor.execute(actualizar, (asientos_ahora, asientos_antes))
            conexion.commit()
            cursor.close()
            conexion.close()
            
            # with open('pasaje.txt', 'w') as pasaje:
            #     pasaje.write('ORIGEN:%s' %self.ciudad_origen)
            #     pasaje.write('DESTINO:%s' %self.ciudad_destino)
            #     pasaje.write('Nombre y apellido:%s + " "+%s' %nombre %apellido)
            #     pasaje.write('DNI: %s' %DNI)
                    
        pass    
    pass   
    
    def crear_caja(self ,donde, x1, y1, ancho1,texto,x2):
        caja = Entry(donde, width=ancho1, fg='black', border=1, bg='white', font=('Mincrosoft Yahei UI Light', 11))
        caja.place(x=x1, y=y1)
        texto_de_caja=Label(donde, text=texto, bg='white', fg='dim gray', font=('Mincrosoft Yahei UI Light', 11, 'bold')).place(x=x2, y=y1)
        return caja, texto_de_caja
pass


        
if __name__ == "__main__":
    root = Tk()
    app = Registro(root)
    root.mainloop()
